$(document).ready(function(){

  $("#gridmode").click(function(){
    $(".txt").hide();
    $(".game").removeClass("line");
  });
  
  $("#listmode").click(function(){
      $(".txt").css({
        'display':'inline-block',
        'width':'320px'
      });
      $(".game").addClass("line");
  });
  
});
