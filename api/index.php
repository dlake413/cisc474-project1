<?php

ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);

date_default_timezone_set("America/New_York");

//This line consults PHPs super global $_SERVER to get the method typically one of GET, POST, PUT, DELETE
$verb = $_SERVER['REQUEST_METHOD'];

//This line gets the full URI including domain path and parameters
$uri = $_SERVER['REQUEST_URI'];

//this uses a PHP function parse_url to get an associative array (think: key value pairs)
//this array has all of the interesting parts of the URL
$url_parts = parse_url($uri);
$path = $url_parts["path"];
//this fetches the parameters namely the blah.com?param1=value1&param2=value2
//$parameters = $url_parts["query"];

//To learn more consult the helpful PHP documentation or do a print_r(whatever) here 
// to see what is happening

//This is some hack-y code to figure out which part of the path is the requested 
//object, it finds "api" inside of "/api/whatever/they/typed" and returns "/whatever/they/typed"
$prefix = "api";
$ind = strpos($path, $prefix);
$request = substr($path, $ind + strlen($prefix));

$dbhandle = new PDO("sqlite:bgg.sqlite") or die("Failed to open DB");
    if (!$dbhandle) die ($error);

//Here I check if the part after api is exactly "/games" anything else is given a 404
//If you wanted a better API you might want to explode the request string by "/" and examine the parts
switch($request){
  case "/total_search":
    if ($verb == "POST"){
      $request = json_decode(file_get_contents("php://input"));
      $keyword = '%';
      $minp = -1;
      $maxp = 100;
      $minpt = -1;
      $maxpt = 1000;
      $query = "SELECT objectname as game, games.objectid, rank, playingtime, minplayers, maxplayers,
            thumbnail, image, description, categories
            from games left join extra on (games.objectid = extra.objectid) 
            where game like :keyword and maxplayers > :minp and maxplayers < :maxp
            and playingtime > :minpt and playingtime < :maxpt";
      if(isset($request->search_keyword)){ $keyword = $request->search_keyword . '%'; };
      if(isset($request->search_min)){ $minp = $request->search_min; };
      if(isset($request->search_max)){ $maxp = $request->search_max; };
      if(isset($request->search_min_pt)){ $minpt = $request->search_min_pt; };
      if(isset($request->search_max_pt)){ $maxpt = $request->search_max_pt; };
      
      
      $statement = $dbhandle->prepare($query);
      $statement->bindParam(":keyword", $keyword, PDO::PARAM_STR, 12);
      $statement->bindParam(":minp", $minp, PDO::PARAM_INT);
      $statement->bindParam(":maxp", $maxp, PDO::PARAM_INT);
      $statement->bindParam(":minpt", $minpt, PDO::PARAM_INT);
      $statement->bindParam(":maxpt", $maxpt, PDO::PARAM_INT);
      
      $statement->execute();
      
      $results = $statement->fetchAll(PDO::FETCH_ASSOC);
        
      header('HTTP/1.1 200 OK');
      header('Content-Type: application/json');
        
      echo json_encode($results);
    }
    else {
      header('HTTP/1.1 404 Not Found');
    }
    break;
  case "/about": //Obtain all things we need to know, like max min 
    if ($verb == "GET"){
      $query = "SELECT max(maxplayers) as maxp, min(minplayers) as minp, max(playingtime) as maxpt, min(playingtime) as minpt from games";
      $statement = $dbhandle->prepare($query);
      $statement->execute();
      $results = $statement->fetchAll(PDO::FETCH_ASSOC);
      header('HTTP/1.1 200 OK');
      header('Content-Type: application/json');
      echo json_encode($results);
    }
    else{
      header('HTTP/1.1 404 Not Found');
    }
    break;
  case "/games":
    if ($verb == "GET") {
      $query = "SELECT objectname as game, games.objectid, rank, playingtime, minplayers, maxplayers,
            thumbnail, image, description, categories
            from games left join extra on (games.objectid = extra.objectid) order by random() limit 0, 10";
      $statement = $dbhandle->prepare($query);
      $statement->execute();
      $results = $statement->fetchAll(PDO::FETCH_ASSOC);
      header('HTTP/1.1 200 OK');
      header('Content-Type: application/json');
      echo json_encode($results);
    } 
    else {
      header('HTTP/1.1 404 Not Found');
    }
    break;
  default:
    header('HTTP/1.1 404 Not Found');
}


?>
