'use strict';


/*
This is the usual opening of an angular "app" which is to define a module 
(In my estimation module and app are the same thing).
most definitions in angular allow "dependency injection", that is, you can state
in the square brackets any other modules, libraries, objects, etc that this module
will rely on.
*/

var myApp = angular.module('sampleapp', []);

/* 
This is the first "Class" that we have made in the traditional Javascript method
In javascript classes are functions and you can instantiate an object by calling
new GameClass() (as below).  Inside the function you will use the keyword "this"
to refer to the object
*/

var GameClass = function(){
  this.game = "A default game";
  this.playingtime = "Unknown";
  // These default values will eventually be replaced
  this.alert = function(){
      alert(this.game + " :: " + this.playingtime + " minutes");
  };
};

//I love debugging to be done in console...

function print(object){
    console.log(object);
}

myApp.controller('GameController', ['$scope', '$http', function($scope, $http) {
    
    $scope.is_init = true;
    
    $http.get('api/about').success(function(data, status, headers, config){
        $scope.players_range = [];
        $scope.playtime_range = [];
        var info = data[0];
        for (var i=parseInt(info.minp); i <= info.maxp; i++){
            $scope.players_range.push(i);
        }
        
        for (var i=parseInt(info.minpt); i <= parseInt(info.maxpt); i += (parseInt(info.maxpt)-parseInt(info.minpt))/10){
            $scope.playtime_range.push(i);
        }
    });
    
    $scope.search_min = -1;
    $scope.search_max = -1;
    $scope.search_min_pt = -1;
    $scope.search_max_pt = -1;
    
    $scope.init = function(){
        $http.get('api/games').success(function(data, status, headers, config){
        $scope.games = [];
        angular.forEach(data, function(game){
            var thisgame = new GameClass();
            angular.extend(thisgame, game);
            $scope.games.push(thisgame);
            }, this);
        });
    }
    
    $scope.total_search = function(){
        var keyword = $scope.search_keyword;
        var search_min = $scope.search_min;
        var search_max = $scope.search_max;
        var search_min_pt = $scope.search_min_pt;
        var search_max_pt = $scope.search_max_pt;
        
        var request = {};
        
        if(keyword != null){
            if(keyword.length >= 4){
                request.search_keyword = keyword;
            }
        }
        
        if(parseInt(search_min) > 0){ request.search_min = search_min; }
        if(parseInt(search_max) > 0){ request.search_max = search_max; }
        if(parseInt(search_min_pt) > 0){ request.search_min_pt = search_min_pt };
        if(parseInt(search_max_pt) > 0){ request.search_max_pt = search_max_pt };
        
        if(Object.keys(request).length > 0){
            $http.post('api/total_search', request).success(function(data, status, headers, configs){
                $scope.games = [];
                angular.forEach(data, function(game){
                    var thisgame = new GameClass();
                    angular.extend(thisgame, game);
                    $scope.games.push(thisgame);
                }, this);
                $scope.is_init = false;
            }).error(function(data, status){
            });
        }
        else {
            if(!$scope.is_init){
                $scope.init();
                $scope.is_init = true;
            }
            return;
        }
    }
    
}]);